<?php
session_start();
$cart = $_SESSION['user_cart'];
$products = [
	[
	 'title' => 'Ноутбук Dell Inspiron',
	 'price' => 4000
	],
	[
	 'title' => 'Ноутбук Asus Vivobook',
	 'price' => 4500
	],
	[
	 'title' => 'Ноутбук HP 250 G5',
	 'price' => 6000
	],
	[
	 'title' => 'Ноутбук Acer Aspire',
	 'price' => 4000
	],
	[
	 'title' => 'Ноутбук Lenovo IdeaPad',
	 'price' => 5000
	],
	[
	 'title' => 'Ноутбук Apple A1466',
	 'price' => 12000
	],
	[
	 'title' => 'Ноутбук Prestigio Smartbook 141A03',
	 'price' => 4200
	],
	[
	 'title' => 'Ноутбук MSI GE72MVR-7RG',
	 'price' => 6500
	],
	[
	 'title' => 'Ноутбук Xiaomi Mi Notebook Air 12 M3',
	 'price' => 5800
	],
	[
	 'title' => 'Ноутбук Razer Stealth',
	 'price' => 7800
	]
];




?>
<!DOCTYPE html>
<html>
<head>
	<title>Shop</title>
	<meta http-equiv="Conten-Type" content="text/html"; charset="utf-8"/>
</head>
<body>
	<?php
		foreach ($products as $id => $product) {?>
	<div>
		<h3><?=$product['title'] ?></h3>
		<p> <?=$product['price'] ?></p>
		<p><a href="cart_add.php?buy_item=<?= $id ?>">Buy</a></p>
	</div>
	<?php } 
		if(isset($_SESSION['user_cart'])){ ?>
	 <div>
		<h3>Cart:</h3>
			<ul>
				<?php foreach($cart as $product_id => $amount): ?> 
					<li>
					<strong>
						<?=$products[$product_id]['title']?>
					</strong>x
					<?=$amount?>
					</li>
				<?php  endforeach;} ?>
			</ul>
		</div>  
		<div><h3><a href="cart_add.php?clear">Clear</a></h3></div> 
   </body>
</html>